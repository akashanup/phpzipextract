<?php
use Aws\S3\S3Client;

require_once $_SERVER['DOCUMENT_ROOT'].'/extract/vendor/autoload.php';
$config = require($_SERVER['DOCUMENT_ROOT'].'/extract/config/config.php');

$s3 = new S3Client([
    'version'     => 'latest',
    'region'      => $config['s3']['region'],
    'credentials' => [
        'key'    => $config['s3']['key'],
        'secret' => $config['s3']['secret'],
    ],
]);
?>
