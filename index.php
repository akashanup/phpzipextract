<?php
use Aws\S3\Exception\S3Exception;
use GuzzleHttp\Client;

class Extract
{
	public function __construct()
	{
		require_once $_SERVER['DOCUMENT_ROOT'] . '/extract/config/start-upload.php';
		$trial_data = [];
		try {
			$message = json_decode(file_get_contents('php://input'), true); 
	    	switch ($message['Type']) {
	    		case 'SubscriptionConfirmation':
	    			$client = new Client();
			        $response = $client->request('GET', $message['SubscribeURL']);
	    			break;
	    		case 'Notification':
					$bucket = $config['s3']['bucket'];
	    			$response = json_decode($message['Message'], true);
	    			$zipKeyName = $response['Records'][0]['s3']['object']['key'];
					$s3Details = ['Bucket' => $bucket, 'Key' => $zipKeyName];
				    $zipFileMetadata = $s3->headObject($s3Details)->toArray();
				    $userId = $zipFileMetadata['Metadata']['user_id'];
				    $ballStatus = json_decode($zipFileMetadata['Metadata']['ball_status'], true);
					$filepath = $_SERVER['DOCUMENT_ROOT'] . '/extract/trial-videos/' . $userId . '.zip';
					$zipfile = file_get_contents($s3->getObjectUrl($bucket, $zipKeyName));
					$fileput = file_put_contents($filepath, $zipfile);
	    			$ball_arr = [];
				    foreach($ballStatus as $key => $ball){
						if(!$ball["is_transcode_publish"]){
							$ball_arr[$key] = $ball["ball_no"];
						}
					}
					$trial_data["BallArray"] = $ball_arr;
					$zip = new ZipArchive;
					$res = $zip->open($filepath);
					if ($res === TRUE) {
						$zip->extractTo('trial-videos/');
						$zip->close();
						$dir = $_SERVER['DOCUMENT_ROOT'] . '/extract/trial-videos/' . $userId;
						$trial_data["isDir"] = is_dir($dir);
						if (is_dir($dir)) {
						  	if ($dh = opendir($dir)) {
								$i = 0;
						    	while ($file = readdir($dh)) {
						    		if (($file == ".") || ($file == "..")) {
            							continue;
						    		}
						    		chmod($file, 0777);
									$trial_data["FileMessage"][$i] = "Filefound.";
						    		$bc_arr = explode('-',$file);
						            if(is_array($bc_arr)) {
						                $lastEl = array_values(array_slice($bc_arr, -1))[0];
						                $ball_count = intval($lastEl);
						            } else {
						            	$ball_count = 0;
						            }
									$trial_data["BallCount"][$i] = $ball_count;
						            if(!in_array($ball_count, $ball_arr)) {
						            	$basename = pathinfo($file,PATHINFO_BASENAME);
								    	$ext = pathinfo($file, PATHINFO_EXTENSION);
								    	$temp_file = $dir."/".$file;
										$s3->putObject([
											'Bucket' => $bucket,
											'Key' => 'trial-videos-uploaded/' . $userId . '/' . $basename,
											'Body' => fopen($temp_file,'rb'),
											'ACL' => 'public-read',
											'Metadata' => array(
										      'user_id' => $userId,
										      'RequestFor' => 'TranscodeFpTrialVideo',
										      'file_name' => $basename
										    )
										]);
										unlink($temp_file);
						            }
						            $i++;
						    	}
							    closedir($dh);
							  	rmdir($dir);
							  	unlink($filepath);
							  	// Remove zip from S3
							  	$s3->deleteObject($s3Details);
								$trial_data["UploadProcess"] = "Upload process complete for user id #" . $userId;
						  	} else {
								$trial_data["DirectoryError"] = 'Unable to open directory '. $dir;
						  	}
						} else {
							$trial_data["DirectoryError"] = $dir. ' directory not found.';
						}
					} else {
					  $trial_data["ZipExtract"] = "Some issue in extracting zip file for user id" . $userId;
					}
	    			break;
	    		case 'UnsubscribeConfirmation':
	    			# code...
	    			break;
	    		default:
	    			# code...
	    			break;
	    	}
		} catch(S3Exception $e){
			$trial_data["S3Exception"] = $e->getMessage();
		} catch (Exception $e) {
			$trial_data["OtherException"] = $e->getMessage();
		} finally {
			$logFilePath = $_SERVER['DOCUMENT_ROOT'] . '/extract/trial_videos.json';
			$jsonInfo = file_exists($logFilePath) ? 
				!empty(file_get_contents($logFilePath)) ? 
					json_decode(file_get_contents($logFilePath)): [] 
						: [];
			array_push($jsonInfo, $trial_data);
			file_put_contents($logFilePath, json_encode($jsonInfo), FILE_APPEND);
		}
		die("Extracted and uploaded!");
	}
}
$extract = new Extract();
?>
